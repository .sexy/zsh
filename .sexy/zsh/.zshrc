#!/usr/bin/env zsh
source ~/.sexy.rc
sexy-dbg '#>>> zsh/.zshrc'

setopt prompt_subst
setopt transient_rprompt
setopt equals
setopt extended_glob
setopt nullglob

#setopt glob_subst

fpath+="${SEXY_ZSH}/completions"

zo=(${_Z_OWNER} $SUDO_USER $USER $LOGNAME $(whoami 2>/dev/null))



typeset -g _Z_DATA="${_Z_DATA:-${SEXY_ZSH_DATA}/z}"
typeset -g _Z_OWNER=$zo[1]
# "${_Z_OWNER:-${SUDO_USER:-${USER:-${LOGNAME:-$(whoami 2>/dev/null)}}}}"
typeset -g _Z_NO_RESOLVE_SYMLINKS="${_Z_NO_RESOLVE_SYMLINKS:-1}"

SEXY_ZSH_COMPD="${SEXY_ZSH_COMPD:-${SEXY_ZSH_DATA}/zcompdump}"
SEXY_ZSH_COMPC="${SEXY_ZSH_COMPC:-${SEXY_ZSH_DATA}/zcompcache}"

typeset -a __deferred_compdefs
compdef () {
    sexy-dbg "  @> +$0 [$*]"
    __deferred_compdefs+=("$*")
}

sexy-src zsh cfg profile
sexy-src zsh cfg zshrc.$SEXY_ZSH_MNGR

autoload -Uz compinit && \
    compinit -C -d "${SEXY_ZSH_COMPD}"

() {
    sexy-dbg '   > Applying deffered compdefs'
    local cdef
    for cdef in $__deferred_compdefs; {
        compdef $cdef
    }
    unset __deferred_compdefs
    sexy-dbg '   < Deffered compdefs applied'
}

zstyle ':completion::complete:*' cache-path "${SEXY_ZSH_COMPC}"

sexy-dbg '#<<< zsh/.zshrc'

